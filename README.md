# PyChessDiag
Project for Computer Science Studies (potential master thesis subject).
Chess diagram recognizer used for my own ChessCoach app - helps chess trainers with setting up the position
for their chess exercises. Uses TensorFlow neural network to learn to recognize chess pieces on a diagram.

### Prerequisites
+ Installed python 3.6
+ Installed required python libs
```angular2html
pip install numpy
pip install tensorflow
pip install scikit-image
```

### Planned features
+ Provide more chess pieces images for better recognition
+ Teach network to recognize whole diagrams (recognition of lines/squares on a chessbaord)
+ Create a backend service with Tensorflow neural network for diagram recognition 
+ Provide the backend for ChessCoach app