import tensorflow as tf
import os
from skimage import io, transform, color
from pathlib import Path
import numpy as np
import matplotlib.pyplot as pieplot


TRAIN_DIAG_PATH = os.path.join(Path().absolute(), "train_diags")
TEST_DIAG_PATH = os.path.join(Path().absolute(), "test_diags")

piece2number = {'NaN': 0, 'white_pawn': 1, 'white_knight': 2, 'white_bishop': 3, 'white_rook': 4, 'white_queen': 5,
                'white_king': 6, 'black_pawn': 7, 'black_knight': 8, 'black_bishop': 9, 'black_rook': 10,
                'black_queen': 11, 'black_king': 12}
number2piece = {value: key for key, value in piece2number.items()}


class TensorNetwork:
    def __init__(self):
        self.x = None
        self.y = None
        self.session = None
        self.correct_predictions = None

    def tf_learn(self, data, label):
        my_graph = tf.Graph()
        with my_graph.as_default():
            self.x = tf.placeholder(dtype=tf.float32, shape=[None, 32, 32])
            self.y = tf.placeholder(dtype=tf.int32, shape=[None])
            # Flattened input data
            flattened = tf.contrib.layers.flatten(self.x)
            # Layer
            logits = tf.contrib.layers.fully_connected(flattened, 13, tf.nn.relu)
            # Loss function
            loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.y, logits=logits))
            # Optimizer
            # optimizer = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)
            optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01).minimize(loss)
            # Logits -> label indexes
            self.correct_predictions = tf.argmax(logits, 1)
            # Accuracy
            accuracy = tf.reduce_mean(tf.cast(self.correct_predictions, tf.float32))

            tf.set_random_seed(1234)
            self.session = tf.Session()

            self.session.run(tf.global_variables_initializer())
            for i in range(1000):
                _, accuracy_val = self.session.run([optimizer, accuracy], feed_dict={self.x: data, self.y: label})

    def tf_predict(self, data, label):
        predicted = self.session.run([self.correct_predictions], feed_dict={self.x: data})[0]
        for i, l in enumerate(label):
            print("Piece: {f} / Prediction: {p}".format(f=number2piece[l], p=number2piece[predicted[i]]))
        self.session.close()


def load_training_data(data_dir):
    training_dirs = [d for d in os.listdir(data_dir)
                     if os.path.isdir(os.path.join(data_dir, d))]

    images = []
    labels = []

    for d in training_dirs:
        current_dir = os.path.join(data_dir, d)
        current_dir_files = [os.path.join(current_dir, f)
                             for f in os.listdir(current_dir) if f.endswith(".jpg")]
        for f in current_dir_files:
            images.append(io.imread(f))
            labels.append(piece2number[d])

    return np.array(images), np.array(labels)


def load_test_data(data_dir):
    current_dir_files = [os.path.join(data_dir, f)
                         for f in os.listdir(data_dir) if f.endswith(".jpg")]
    images = []
    labels = []
    for f in current_dir_files:
        no_digits = ''.join(i for i in os.path.basename(f) if not i.isdigit())
        no_digits = no_digits.replace('.jpg', '')
        images.append(io.imread(f))
        labels.append(piece2number[no_digits])

    return images, labels


def prepare_data(data):
    images_u = [transform.resize(image, (32, 32)) for image in data]
    images_u = np.array(images_u)
    images_gray = color.rgb2gray(np.array(images_u))
    return images_gray


# Helping function to show prepared images and their shape
def plot_data(data):
    random_data = [1, 5, 17, 29]
    for i in range(len(random_data)):
        pieplot.subplot(1, 4, i+1)
        pieplot.axis('off')
        pieplot.imshow(data[random_data[i]], cmap='gray')
        pieplot.subplots_adjust(wspace=0.5)
        pieplot.show()
        print("shape: {0}, min: {1}, max: {2}".format(data[random_data[i]].shape,
                                                      data[random_data[i]].min(),
                                                      data[random_data[i]].max()))


def main(_):

    network = TensorNetwork()
    # Load data
    training_images, training_labels = load_training_data(TRAIN_DIAG_PATH)
    testing_images, testing_labels = load_test_data(TEST_DIAG_PATH)

    # Prepare training data
    prepared_training_images = prepare_data(training_images)

    # Set network to learn
    network.tf_learn(prepared_training_images, training_labels)

    # Prepare testing data
    prepared_testing_images = prepare_data(testing_images)

    # Make a predictions
    network.tf_predict(prepared_testing_images, testing_labels)


if __name__ == "__main__":
    tf.app.run()

